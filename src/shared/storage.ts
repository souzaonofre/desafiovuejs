import { LocalStorage } from "quasar";
import { FilterDataType } from './api';

type StorageRootType = {
  filters: Array<FilterDataType>
};

const STORAGE_ROOT_KEY = 'speedIoData';

const emptyRootData: StorageRootType = {
  filters: []
};

function encodeData(data: StorageRootType): string {
  try {
    const buff: Buffer = Buffer.from(JSON.stringify(data), 'utf8');
    return buff.toString('base64');
  } catch (error) {
    return '';
  }
}

function decodeData(hash: string|null): StorageRootType {
  try {
    const dataIn: string = hash ?? '';
    const buff: Buffer = Buffer.from(dataIn, 'base64');
    const dataOut: string = buff.toString('utf8');
    return JSON.parse(dataOut);
  } catch (error) {
    return emptyRootData;
  }
}

function getRootData(): StorageRootType {
  try {
    if (!LocalStorage.has(STORAGE_ROOT_KEY)) {
      const rootData: string = encodeData(emptyRootData);
      LocalStorage.set(STORAGE_ROOT_KEY, rootData);
      return emptyRootData;
    }
    const storageData: string|null = LocalStorage.getItem(STORAGE_ROOT_KEY);
    return decodeData(storageData);
  } catch (err) {
    return emptyRootData;
  }
}

function setRootData(data: StorageRootType) {
  try {
    if (!LocalStorage.has(STORAGE_ROOT_KEY)) {
      const rootData: string = encodeData(emptyRootData);
      LocalStorage.set(STORAGE_ROOT_KEY, rootData);
      return false;
    }
    const storageData = encodeData(data);
    LocalStorage.set(STORAGE_ROOT_KEY, storageData);
    return true;
  } catch (err) {
    return false;
  }
}

function saveFiltersData(filtersData: Array<FilterDataType>) : boolean {
  try {
    if (filtersData.length <= 0) {
      return false;
    }
    const rootData: StorageRootType = getRootData();
    rootData.filters = filtersData;

    setRootData(rootData);
    return true;
  } catch (err) {
    return false;
  }
}

function loadFiltersData(): Array<FilterDataType> {
  try {
    const rootData: StorageRootType = getRootData();
    return rootData.filters;
  } catch (err) {
    return [];
  }
}

export default {
  saveFiltersData,
  loadFiltersData
};
