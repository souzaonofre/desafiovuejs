const FILTERS_URL = 'https://filters.dev.speedio.com.br/api/v3/filters.json';

export type FilterDataType = {
  filters: Array<FiltersGroupObjType>
}

export type FiltersGroupObjType = {
  id: number,
  groupName: string,
  groupTag: string,
  language: string,
  filters: Array<FilterObjType>
}

export type FilterObjType = {
  id: number,
  title: string,
  groupTag: string,
  queryField?: string,
  queryType?: string,
  filterOptions: Array<FilterOptions>
  typeOptions?: Array<any>
}

export type FilterOptions = {
  subline: string,
  value: string,
  label: string,
  type: string,
  queryfield: string,
  tags: string,
}

async function httpGet(request: RequestInfo): Promise<FilterDataType> {
  const opts: RequestInit = {
    method: 'GET',
    headers: [],
  };
  const response: Response = await fetch(request, opts);
  const data: FilterDataType = await response.json();
  return data;
}


function getGroupTagFilters(groupName: string, tagName:string, filtersData: FilterDataType): Array<FilterObjType> {
  const filters: Array<FilterObjType> = [];
  filtersData.filters.forEach((item: FiltersGroupObjType) => {
    filters.concat(item.filters);
  });
  return filters;
}

export async function getFilterOptions(groupName: string, tagName:string): Promise<Array<FilterOptions>> {
  const filtersData: FilterDataType = await httpGet(FILTERS_URL);
  const filters: Array<FilterObjType> = getGroupTagFilters(groupName, tagName, filtersData);

  const filterOptions: Array<FilterOptions> = [];
  filters.forEach((item: FilterObjType) => {
    filterOptions.concat(item.filterOptions);
  });

  return filterOptions;
}
